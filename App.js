import React, {Component} from 'react';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import reducer from "./src/store/reducer";
import Application from "./Application";

const store = createStore(reducer, applyMiddleware(thunk));

const App = () => (
    <Provider store={store}>
      <Application />
    </Provider>
);

export default App;