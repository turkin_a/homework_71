import React from 'react';
import {Text, View, StyleSheet, Image} from "react-native";

const Item = props => (
  <View style={styles.item}>
    <Image resizeMode="contain" source={{uri: props.icon}} style={styles.image} />
    <Text>{props.title}</Text>
  </View>
);

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
    backgroundColor: '#eee',
    marginBottom: 10,
    padding: 10,
    paddingRight: 70,
    borderRadius: 10
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 10,
    borderRadius: 10
  }
});

export default Item;