import axios from '../../axios-pictures';

export const LOAD_PICTURES = 'LOAD_PICTURES';

export const loadPictures = pictures => {
  return {type: LOAD_PICTURES, pictures};
};

export const getPictures = () => {
  return dispatch => {
    axios.get('/pics.json').then(response => {
      dispatch(loadPictures(response.data));
    }, error => {
      alert(error);
    });
  }
};

export const getMorePictures = nextList => {
  return dispatch => {
    axios.get(`/pics.json?count=25&after=${nextList}`).then(response => {
      dispatch(loadPictures(response.data));
    }, error => {
      alert(error);
    });
  }
};