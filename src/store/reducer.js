import {LOAD_PICTURES} from "./actions";

const initialState = {
  pictures: [],
  nextList: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_PICTURES:
      const newPictures = action.pictures.data.children.map(item => ({
        key: item.data.id,
        icon: item.data.thumbnail,
        title: item.data.title
      }));
      const pictures = [...state.pictures, ...newPictures];
      return {...state, pictures, nextList: action.pictures.data.after};
    default:
      return state;
  }
};

export default reducer;