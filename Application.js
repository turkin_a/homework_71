import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";
import {getMorePictures, getPictures} from "./src/store/actions";
import Item from "./src/components/Item";

class Application extends Component {
  componentDidMount() {
    this.props.loadContent();
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.pictures}
          onEndReachedThreshold={0.05}
          onEndReached={() => this.props.loadMore(this.props.nextList)}
          renderItem={(info) => (
            <Item title={info.item.title}
                  icon={info.item.icon}
            />
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingHorizontal: 10
  }
});

const mapStateToProps = state => {
  return {
    pictures: state.pictures,
    nextList: state.nextList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadContent: () => dispatch(getPictures()),
    loadMore: (nextList) => dispatch(getMorePictures(nextList))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Application);